#!/usr/bin/env bash
set -e

# go script directory 
cd $( dirname "${0}" )

GREEN='\033[0;32m'
RED='\033[0;31m'
LINEBEGIN='\033[0K\r'
NC='\033[0m'

if [ $(hostname -d | cut -d'.' -f2) != "grid5000" ]; then
    echo -e "${RED}Sorry only Grid'5000 platform is supported (contact the oar-team if need help to adapt)${NC}"
    exit 1
fi

usage() { echo "Usage: $0 [-n nb_nodes -w walltime_hour] [start|stop|connect|clean|register]" 1>&2; exit 1; }

#Variables below can be overwritten by gitlab-ci.conf or cli arguments
NB_NODES=1
WALLTIME_HOUR=1
BUILD_OAR_OPTIONS=()
NXC_OAR_OPTIONS=()

source ./gitlab-ci.conf

while getopts "hn:w:" o; do
    case "${o}" in
        h)
            usage;;
        n)    
            NB_NODES=${OPTARG}
            ;;
        w)
            WALLTIME_HOUR=${OPTARG}
            ;;
    esac
done

shift $(expr $OPTIND - 1 )

if [ $# -eq 0 ]; then
    command="start"
else
    command=$1
fi
    
if [ $command == "stop" ] ||  [ $command == "clean" ]; then
    if [ ! -f var/NXC_OAR_JOB_ID ] && [ ! -f var/RUNNER_OAR_JOB_ID ] ; then
        echo -e "${RED}Cannot find running instance${NC}"
        exit 1
    fi    
    echo -e "${GREEN}Stop NXC gitlab-runner${NC}"
    if [ -f var/NXC_OAR_JOB_ID ] ; then
        NXC_OAR_JOB_ID=$(cat var/NXC_OAR_JOB_ID)
        echo "Stop job fot NXC deployments (NXC_OAR_JOB_ID): $NXC_OAR_JOB_ID"
        oardel $NXC_OAR_JOB_ID &>null || true
        rm var/NXC_OAR_JOB_ID
    fi
    if [ -f var/RUNNER_OAR_JOB_ID ] ; then
        RUNNER_OAR_JOB_ID=$(cat var/RUNNER_OAR_JOB_ID)
        echo "Stop job for NXC deployments (RUNNER_OAR_JOB_ID): $RUNNER_OAR_JOB_ID"
        oardel $RUNNER_OAR_JOB_ID &>null || true
        rm var/RUNNER_OAR_JOB_ID
    fi
    if [ $command == "clean" ]; then
        rm -rf var || true
        rm -rf builds || true
    fi    
    exit 
fi

if [ $command == "connect" ]; then
    if [ ! -f var/RUNNER_OAR_JOB_ID ]; then
        echo -e "${RED}Cannot connect var/RUNNER_OAR_JOB_ID file does not exists${NC}"
        echo "Is NXC gitlab-runner started ?"
        exit 1
    fi
    echo -e "${GREEN}Connect to NXC gitlab-runner${NC}"
    oarsub -C $(cat var/RUNNER_OAR_JOB_ID) 
    exit
fi

if [ ! -f bin/gitlab-runner ]; then
    echo "Install gitlab-runner"
    mkdir -p bin
    curl -L --output bin/gitlab-runner "https://gitlab-runner-downloads.s3.amazonaws.com/latest/binaries/gitlab-runner-linux-amd64"
    chmod 755 bin/gitlab-runner
fi

if [ ! -f $HOME/.gitlab-runner/config.toml ] || [ $command == "register" ]; then
   echo "Register G5K Runner"
   ./bin/gitlab-runner register \
     --non-interactive \
     --url $GITLAB_URL \
     --registration-token $TOKEN \
     --name g5k-runner \
     --tag-list g5k \
     --executor shell
   if [ $command == "register" ]; then
       exit
   fi    
fi

if [ -f var/NXC_OAR_JOB_ID ] || [ -f var/RUNNER_OAR_JOB_ID ] ; then
    echo -e "${RED}Cannot start a previous instance seems running${NC}"
    echo "Use stop command before."
    exit 1
fi

echo -e "${GREEN}Starting NXC gitlab-runner${NC}"
echo -e "with ${GREEN}$NB_NODES${NC} nodes for deployment and ${GREEN}${WALLTIME_HOUR}h${NC} walltime"

# echo nxc ab nix version, will exit one command is absent
nxc --version
nix --version

#
mkdir -p var/tmp

# Launch job for nxc deployment
export $(oarsub "${NXC_OAR_OPTIONS[@]}" \
                -l "nodes=$NB_NODES,walltime=$WALLTIME_HOUR:0:0" \
                -O var/NXC_OAR_%jobid%.stdout \
                -E  var/NXC_OAR_%jobid%.stderr \
                "$(nxc helper g5k_script) ${WALLTIME_HOUR}h" | grep OAR_JOB_ID
)
NXC_OAR_JOB_ID=$OAR_JOB_ID
echo "Jobid for NXC deployments (NXC_OAR_JOB_ID) $NXC_OAR_JOB_ID" 
echo $NXC_OAR_JOB_ID > var/NXC_OAR_JOB_ID

nxc_oar_job_stdout=var/NXC_OAR_${NXC_OAR_JOB_ID}.stdout

# Wait for nxc_oar_job_stdout creation with asked number of nodes
sec=0
until [ -f $nxc_oar_job_stdout ] && [ $( wc -l < $nxc_oar_job_stdout) -eq $NB_NODES ]
do
    echo -ne "Waiting for file ${nxc_oar_job_stdout} with ${NB_NODES} nodes (${GREEN}${sec}s${NC})${LINEBEGIN}"
    sleep 1
    sec=$((sec + 1))
done
echo
echo "Launch gitlab-runner job"
# Launch gitlab-runner job
export $(oarsub "${BUILD_OAR_OPTIONS[@]}" \
                -l "nodes=1,walltime=$WALLTIME_HOUR:0:0" \
                -O var/RUNNER_OAR_%jobid%.stdout \
                -E  var/RUNNER_OAR_%jobid%.stderr \
                "bin/gitlab-runner run" | grep OAR_JOB_ID
    )
RUNNER_OAR_JOB_ID=$OAR_JOB_ID
echo "Jobid for gitlab-runner (RUNNER_OAR_JOB_ID) $RUNNER_OAR_JOB_ID"
echo $RUNNER_OAR_JOB_ID > var/RUNNER_OAR_JOB_ID

