Execute Gilab's Pipelines for  Nixos-Compose based project on Grid'5000
===============================================================================================

# Requirement
From General `Setting / CI/CD / Runners / Expands`
1. **Url** of Gitlab
2. Registration **token**

# Installation on Grid'5000
1. Clone the repo
```bash
git git@gitlab.inria.fr:nixos-compose/nxc-gitlab-g5k.git
```
# Setting and usage
1. Remane `gitlab-ci.conf.example` to `gitlab-ci.conf`
2. Adapt `gitlab-ci.conf` by setting  **url**, **token** and change defaut if needed.
```bash
BUILD_OAR_OPTIONS=(-t exotic -p "cluster='yeti' or cluster='troll'")
NXC_OAR_OPTIONS=(-t exotic -p "cluster='yeti' or cluster='troll'")
NB_NODES=5       
GITLAB_URL=https://gricad-gitlab.univ-grenoble-alpes.fr
TOKEN=xxxxxxxxxxxxxxxxxxxxxxxxxxxx 
```
3. Register (only once by project)
```bash
./nxc-gitlab.sh register
```
**Note:** It will automatically download ```gitlab-runner``` from https://gitlab-runner-downloads.s3.amazonaws.com (link provided by Gitlab project) and save in `./bin` directory.  

4. Launch
```bash
./nxc-gitlab.sh 
#or 
./nxc-gitlab.sh run
```
**Notes:** use option `-n` to override the number of_nodes and `-w` for wwalltime in hour unit.

5. Stop and clean
```bash
# Stop will delete oar jobs (build/deploy)
./nxc-gitlab.sh stop
# Same as 
./nxc-gitlab.sh run
```

